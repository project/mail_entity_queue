<?php

namespace Drupal\mail_entity_queue;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the mail entity queue entity type.
 */
class MailEntityQueueItemViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $base_table = $this->entityType->getBaseTable() ?: $this->entityType->id();

    $data[$base_table]['source_entity_label'] = [
      'title' => $this->t('Created to: Entity label'),
      'help' => $this->t('The label of entity to which this queue item was created.'),
      'field' => [
        'id' => 'mail_entity_queue_created_to_label',
      ],
    ];

    // Reverse relationship on the "entity_type" and "entity_id" columns, i.e.
    // from an arbitrary entity to customization that have been created to it.
    foreach ($this->entityTypeManager->getDefinitions() as $definition) {
      if ($definition instanceof ContentEntityTypeInterface) {
        $relationship = [
          'base' => $base_table,
          'field' => $definition->getKey('id'),
          'base field' => 'entity_id',
          'id' => 'standard',
          'extra' => [
            ['field' => 'entity_type', 'value' => $definition->id()],
          ],
        ];

        if ($definition->getDataTable()) {
          $foreign_table = $definition->getDataTable();
          $relationship['extra'][] = ['field' => 'langcode', 'left_field' => 'langcode'];
        }
        else {
          $foreign_table = $definition->getBaseTable();
        }

        $data[$foreign_table][$base_table] = [
          'title' => $this->t('Mail queue items'),
          'help' => $this->t('Mail queue items created to an entity.'),
        ];

        $data[$foreign_table][$base_table]['relationship'] = $relationship;
      }
    }

    return $data;
  }

}
