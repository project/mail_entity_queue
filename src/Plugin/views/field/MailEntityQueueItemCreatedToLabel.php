<?php

namespace Drupal\mail_entity_queue\Plugin\views\field;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Display entity label which a queue item was created to.
 *
 * @ViewsField("mail_entity_queue_created_to_label")
 */
class MailEntityQueueItemCreatedToLabel extends FieldPluginBase {

  use MailEntityQueueItemCreatedToTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->languageManager = $container->get('language_manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defineOptions() {
    $options = parent::defineOptions();

    $options['link'] = ['default' => TRUE];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Link to the source entity'),
      '#description' => $this->t('Whether to output this field as a link to the source entity.'),
      '#default_value' => $this->options['link'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $build = [];

    $source_entity = $this->getSourceEntity($values);
    if (!$source_entity) {
      return $build;
    }

    $source_entity = $this->getEntityTranslation($source_entity, $values);

    if (isset($source_entity)) {
      $access = $source_entity->access('view', NULL, TRUE);
      $build['#access'] = $access;
      if ($access->isAllowed()) {
        if ($this->options['link']) {
          $build['entity_label'] = $source_entity->toLink()->toRenderable();
        }
        else {
          $build['entity_label'] = [
            '#plain_text' => $source_entity->label(),
          ];
        }
        $cache = CacheableMetadata::createFromObject($source_entity);
        $cache->applyTo($build);
      }
    }
    return $build;
  }

}
