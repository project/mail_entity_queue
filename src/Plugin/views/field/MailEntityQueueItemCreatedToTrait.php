<?php

namespace Drupal\mail_entity_queue\Plugin\views\field;

use Drupal\views\Entity\Render\EntityTranslationRenderTrait;
use Drupal\views\ResultRow;

/**
 * Fields regarding source entity.
 */
trait MailEntityQueueItemCreatedToTrait {

  use EntityTranslationRenderTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing, just override the parent.
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeId() {
    return $this->getSourceEntity($this->getView()->result[$this->getView()->row_index])->getEntityTypeId();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityTypeManager() {
    return $this->entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  protected function getLanguageManager() {
    return $this->languageManager;
  }

  /**
   * {@inheritdoc}
   */
  protected function getView() {
    return $this->view;
  }

  /**
   * Retrieve the "created to" entity from a result row.
   *
   * @param \Drupal\views\ResultRow $row
   *   The result row.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Source entity of this submission is submitted to or NULL should it not
   *   have one
   */
  protected function getSourceEntity(ResultRow $row) {
    /** @var \Drupal\mail_entity_queue\Entity\MailEntityQueueItemInterface $entity */
    $entity = $this->getEntity($row);

    return $entity->getSourceEntity();
  }

}
